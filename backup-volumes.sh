#/usr/bin/env bash

set -ex

declare -A volumes
volumes[persist]=/opt/bitnami/persist
volumes[certificates]=/opt/bitnami/letsencrypt/certificates
volumes[accounts]=/opt/bitnami/letsencrypt/accounts

function backup {
    for name in "${!volumes[@]}"; do
        path=${volumes[${name}]}
        docker run \
            --rm \
            --volumes-from gateway \
            -v $(pwd)/backup:/backup \
            ubuntu \
            tar cfa \
            /backup/backup-${name}-$(date +%F).tgz \
            ${path}
    done
}

function list {
    for file in $(ls -1 backup); do
        tar tf backup/${file}
    done
}

function restore {
    echo "Not implemented."
    exit 1
}

if [ "$1" == "ls" ]; then
    list
elif [ "$1" == "backup" ]; then
    backup
elif [ "$1" == "restore" ]; then
    restore
else
    echo "Usage"
    echo "./backup-volumes [ls|backup|restore]"
fi
