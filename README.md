### Setup

Use volumes for anything you want to persist:

  - `/opt/bitnami/persist`

    Make sure that there is a `le_domains.txt` (even if empty), if you mount
    a volume for `persist`.

- `/opt/bitnami/letsencrypt/accounts`

    Reuse account to avoid account creation rate limit of 10 accounts per 3h.
    There still applies the main issuing rate limit of 50 certificates a week.
  
  - `/opt/bitnami/letsencrypt/certificates`
    
    This is where letsencrypt writes the certificates. When generating multi-domain
    certificates, the file is named after the first domain given. In the below `Build`
    example, the files will be named `dev.helfa.org.(crt|key|json|issure.crt)`

### Make

The make targets all use docker-compose.

#### Targets
| target | description |
|:-|:-|
| build | Build the docker image |
| up | Start gateway |
| install-cert |  Request a new certificate for build argument $GATEWAY_DOMAINS |
| renew-cert |  Read le_domains.txt from the persist volume and renew certificate with these domains |
| shell |  Enter a shell inside the running container |
| debug |  Enter a shell inside a new container, run from gateway:latest |
| help | Prints usage info |

#### Env

You must configure the container with a `./.env` file.

| var | description | example |
|:-|:-|:-|
| GATEWAY_UID | UID of the non-root user in the container | `4000` |
| GATEWAY_GID | GID of the non-root user in the container | `4000` |
| GATEWAY_DOMAINS | A comma separated list of domains this instance responds to | `dev.abc.org,api.abc.org` |
| GATEWAY_LE_EMAIL | Email address for Letsencrypt | `your@mail.com` |
| GATEWAY_LE_TEST | Use Letsencrypt's staging server to perform requests | `1` |
