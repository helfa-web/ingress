SHELL := /bin/bash

ifneq (${fg}, 1)
	DETACH_OPTION=-d
endif

.PHONY: build up .exec
build:
	docker-compose build gateway
up:
	docker-compose up ${DETACH_OPTION} gateway 
update-config-and-install-certs:
	make build && docker-compose stop && make install-cert && docker-compose up -d
update-config:
	make build && docker-compose up -d --force-recreate
.exec:
	docker-compose exec gateway ${CMD}
.run:
	docker-compose run --rm --service-ports gateway ${CMD}

.PHONY: debug
debug:
	docker-compose run gateway /bin/bash

.PHONY: shell 
shell: CMD = /bin/bash
shell: .exec

.PHONY: install-cert
install-cert: CMD = /opt/bitnami/letsencrypt/scripts/install-cert.sh
install-cert: .run

.PHONY: renew-cert
renew-cert: CMD = /opt/bitnami/letsencrypt/scripts/renew-cert.sh
renew-cert: .exec

.PHONY: help
.SILENT: help
help:
	echo -e "usage: make <target> [fg=]\n"
	echo "  <target>              'build','up', 'shell', 'debug', 'install-cert', 'renew-cert'"
	echo "  [fg=1]                Run in foreground, i.e. dont '-d'etach when bringing services 'up'"
