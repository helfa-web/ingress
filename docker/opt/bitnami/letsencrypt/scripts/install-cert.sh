#!/usr/bin/env bash

shopt -s nullglob

[ -z $BITNAMI_ROOT_DIR ] && . /opt/bitnami/scripts/nginx-env.sh

MODULE="Letsencrypt|install"
. $BITNAMI_ROOT_DIR/scripts/liblog.sh

LE_DIR=$BITNAMI_ROOT_DIR/letsencrypt
CERT_FILES=( $LE_DIR/certificates/* )
LEGO_BIN=$LE_DIR/lego
DOMAINS_OPTIONS=""
SERVER_OPTION=""

if [ ${#CERT_FILES[@]} -gt 1 ]; then
  log "Certificate already installed:"
  $LEGO_BIN --path=$LE_DIR list
  # silent exit when running in a CI job
  [ -n "$CI" ] && exit 0

  read -n1 -p "[Letsencrypt|install] Overwrite y/N? " OVERWRITE_CERT
  echo ""
  if [ "$OVERWRITE_CERT" != "y" ]; then
    log "Aborting."
    exit 0
  fi
fi

# Turn the list of domains in $LE_DOMAINS (e.g. "domain.com,next.domain.com,...")
# into lego --domains options
[ -z $LE_DOMAINS ] && \
  log "Need a list of domains in \$LE_DOMAINS" && \
  exit 1
IFS=',' read -r -a DOMAINS_ARRAY <<< $LE_DOMAINS
for DOMAIN in ${DOMAINS_ARRAY[@]}; do
  DOMAINS_OPTIONS+=" --domains=$DOMAIN"
done

if [ -n "$LE_TEST" ]; then
  log "Using Letsencrypt staging server"
  SERVER_OPTION="--server https://acme-staging-v02.api.letsencrypt.org/directory"
fi

$LEGO_BIN \
  --accept-tos \
  --http \
  --http.port=:8080 \
  --email=$LE_EMAIL \
  --path=$LE_DIR \
  $DOMAINS_OPTIONS \
  $SERVER_OPTION \
  run
