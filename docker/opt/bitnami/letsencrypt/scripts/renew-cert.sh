#!/usr/bin/env bash

[ -z $BITNAMI_ROOT_DIR ] && . /opt/bitnami/scripts/nginx-env.sh

MODULE="Letsencrypt|renew"
. $BITNAMI_ROOT_DIR/scripts/liblog.sh

LE_DIR=$BITNAMI_ROOT_DIR/letsencrypt
LE_DOMAINS_FILE=$BITNAMI_ROOT_DIR/persist/le_domains.txt
LEGO_BIN=$LE_DIR/lego
DOMAINS_OPTIONS=""
SERVER_OPTION=""

LEGO_LIST=`$LEGO_BIN --path=$LE_DIR list`
log "Renewing:"
log $LEGO_LIST

if [[ ! $LEGO_LIST =~ "Found the following certs:" ]]; then
  log "No certificates to renew -> Aborting"
  exit 1
fi

# Turn the list of domains in le_domains.txt
# into lego --domains options
IFS=',' read -r -a DOMAINS_ARRAY <<< `cat $LE_DOMAINS_FILE`
for DOMAIN in ${DOMAINS_ARRAY[@]}; do
  DOMAINS_OPTIONS+=" --domains=$DOMAIN"
done

if [ -n "$LE_TEST" ]; then
  log "Using Letsencrypt staging server"
  SERVER_OPTION="--server https://acme-staging-v02.api.letsencrypt.org/directory"
fi

$LEGO_BIN \
  --accept-tos \
  --http \
  --http.webroot=$BITNAMI_ROOT_DIR/persist \
  --email=$LE_EMAIL \
  --path=$LE_DIR \
  $DOMAINS_OPTIONS \
  $SERVER_OPTION \
  renew
