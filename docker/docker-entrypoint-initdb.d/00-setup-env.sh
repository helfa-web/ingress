MODULE="Letsencrypt|Setup"
. $BITNAMI_ROOT_DIR/scripts/liblog.sh

info "Setting up environment for Letsencrypt"
export LE_DIR="$BITNAMI_ROOT_DIR/letsencrypt"
export LE_CERT_DIR="$LE_DIR/certificates"
export LE_DOMAINS_FILE="$BITNAMI_ROOT_DIR/persist/le_domains.txt"
export LAST_DOMAINS=""

if [ -f $LE_DOMAINS_FILE ]; then
  LAST_DOMAINS=`cat $LE_DOMAINS_FILE`
else
  touch $LE_DOMAINS_FILE
fi

IFS=',' read -r -a LE_DOMAINS_ARRAY <<< $LE_DOMAINS
