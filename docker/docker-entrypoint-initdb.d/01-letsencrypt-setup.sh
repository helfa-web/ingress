#!/usr/bin/env bash

# abort on any errors
set -e

MODULE="Letsencrypt|setup"
. $BITNAMI_ROOT_DIR/scripts/liblog.sh

# - read last seen domains
# - compare to current $LE_DOMAINS
#   - same: exit 0
#   - diff:
#     - read and fill well-known.conf.tpl
#     - install well-known.conf to server_blocks
#     - update last seen domains

# always update the domains file
echo $LE_DOMAINS > $LE_DOMAINS_FILE

if [ "$LAST_DOMAINS" == "$LE_DOMAINS" ]; then
   log "Unchanged \$LE_DOMAINS. Not requesting a new certificate."
else
  log "Installing certs for: $LE_DOMAINS"
  $LE_DIR/scripts/install-cert.sh
fi

log "Linking certificate"
ln -fs $LE_CERT_DIR/$LE_DOMAINS_ARRAY.crt /certs/server.crt
ln -fs $LE_CERT_DIR/$LE_DOMAINS_ARRAY.key /certs/server.key
# need issuer cert for OSCP
ln -fs $LE_CERT_DIR/$LE_DOMAINS_ARRAY.issuer.crt /certs/issuer.crt
