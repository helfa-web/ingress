#!/usr/bin/env bash

set -e

MODULE="nginx|activate-blocks"
. $BITNAMI_ROOT_DIR/scripts/liblog.sh

AVAILABLE_BLOCKS=$BITNAMI_ROOT_DIR/server_blocks
ACTIVE_BLOCKS=$NGINX_CONF_DIR/server_blocks

for DOMAIN in ${LE_DOMAINS_ARRAY[@]}; do
  BLOCK_CONF_FILE=$AVAILABLE_BLOCKS/$DOMAIN.conf
  ACTIVE_CONF_LINK=$ACTIVE_BLOCKS/$DOMAIN.conf

  if [ -r $BLOCK_CONF_FILE ]; then
    info "Activating server block for: $DOMAIN"
    ln -f -s $BLOCK_CONF_FILE $ACTIVE_CONF_LINK
  else
    warn "No server block found for: $DOMAIN"
  fi
done
