FROM bitnami/nginx:1.22

ARG UID
ARG GID

USER 0:0

# will need cron for automatic renewal
RUN install_packages wget cron curl
# https://docs.bitnami.com/general/how-to/generate-install-lets-encrypt-ssl/#alternative-approach
RUN curl -Ls https://api.github.com/repos/xenolf/lego/releases/latest | \
      grep browser_download_url | \
      grep linux_amd64 | \
      cut -d '"' -f 4 | \
      wget -O /tmp/lego.tar.gz -qi -
RUN tar xf /tmp/lego.tar.gz
RUN mkdir -p /opt/bitnami/letsencrypt
RUN mv lego /opt/bitnami/letsencrypt/lego

ARG DOMAINS
ENV LE_DOMAINS $DOMAINS

WORKDIR /opt/bitnami
ADD ./docker/docker-entrypoint-initdb.d/* /docker-entrypoint-initdb.d/
RUN mkdir -p ./letsencrypt/certificates ./letsencrypt/accounts

# Consideration for the order of these is caching, i.e.
# files that never or less often change should come first.
ADD ./docker/opt/bitnami/letsencrypt/dhparam.txt ./letsencrypt/dhparam
ADD ./docker/opt/bitnami/nginx/conf/ssl.conf ./nginx/conf/ssl.conf
ADD ./docker/opt/bitnami/nginx/conf/nginx.conf ./nginx/conf/nginx.conf
ADD ./docker/opt/bitnami/letsencrypt/scripts/renew-cert.sh /etc/cron.monthly/renew-cert
ADD ./docker/opt/bitnami/letsencrypt/scripts ./letsencrypt/scripts
ADD ./server_blocks ./server_blocks

RUN chown ${UID}:${GID} -R /opt/bitnami
RUN chown ${UID}:${GID} -R /certs
USER ${UID}:${GID}
