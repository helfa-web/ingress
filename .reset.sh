echo "This action is HIGHLY DESTRUCTIVE, performing a complete reset of the setup. It will"
echo " - remove container 'gateway'"
echo " - remove the named volumes 'gateway_persist' and 'gateway_certificates'"
echo " - remove image gateway:latest"

read -n1 -r -p "Sure you want to do this? [y/N]: " GO_ON
echo ""
# exit code 130 is commonly returned on C-c terminations
# use it to indicate user-requested failure state
[ "$GO_ON" != 'y' ] && echo -e "\nAborting." && exit 130

docker-compose down
docker rm -f gateway
	docker volume rm gateway_persist
	docker volume rm gateway_certificates
	docker image rm gateway:latest
	make build
